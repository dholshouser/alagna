#ifndef __TELEMETRY_H__
#define __TELEMETRY_H__
#include "Arduino.h"
#include "application.h"
#include <NilFIFO.h>

class Telemetry : public Application
{

private:
  
  typedef struct tlm_soh_n
  {
    pkt_hdr_t hdr;
    byte tlm_registered;
    unsigned int bytes_sent;
  } tlm_soh_t;
  
  typedef struct tlm_table_n
  {
    pkt_t* pkt;
    byte apid;
    byte pktid;
    int  rate; // TBD: This should become some kind of Hertz rate. For now, milliseconds between sends.
    byte size;
    int  last_send_time;
  } tlm_table_t;
  
  // Global variable to hold State of Health tlm packet.
  tlm_soh_t myTlmSoh;
  tlm_table_t TlmTable[TLM_TABLE_SIZE];
  
  // FIFO with 20 tlm pkts.
  NilFIFO<pkt_t*, 20> TlmFifo;

public:
  Telemetry(apid_e apid);

  bool PushPkt(pkt_t* pkt);
  void Initialize(void);
  
  void RegisterTlm(apid_e apid, byte pktid, pkt_t* pkt, byte size, int rate);

  void Process();
  void ProcessTlm();
  cmd_rsp_e ProcessCmd(pkt_t& pkt);
};

#endif
