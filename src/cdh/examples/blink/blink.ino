#include <NilRTOS.h>

//// Use tiny unbuffered NilRTOS NilSerial library.
//#include <NilSerial.h>
//
//// Macro to redefine Serial as NilSerial to save RAM.
//// Remove definition to use standard Arduino Serial.
//#define Serial NilSerial

#include "cdh.h"
#include "application.h"
#include "command.h"
#include "telemetry.h"
#include "blink.h"


///////////////////////////////////////////////////////////////
// Core apps. These are required.
Command Cmd(APID_CMD);
Telemetry Tlm(APID_TLM);
///////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
// Project apps. Initialize project apps here.
Blink Blinkie(APID_BLINK);
///////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
// Setup threads for all the apps in the project.
NIL_WORKING_AREA(waCmdThread, DEFAULT_THREAD_STACK_SIZE);
NIL_THREAD(CmdThread, arg){  Cmd.Execute(); }

NIL_WORKING_AREA(waIOThread, DEFAULT_THREAD_STACK_SIZE);
NIL_THREAD(IOThread, arg) { Tlm.ProcessTlm(); }

NIL_WORKING_AREA(waTlmThread, DEFAULT_THREAD_STACK_SIZE);
NIL_THREAD(TlmThread, arg) { Tlm.Execute(); }

NIL_WORKING_AREA(waBlinkThread, DEFAULT_THREAD_STACK_SIZE);
NIL_THREAD(BlinkThread, arg) { Blinkie.Execute(); }
///////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
/*
 * Threads static table, one entry per thread.  A thread's priority is
 * determined by its position in the table with highest priority first.
 * 
 * These threads start with a null argument.  A thread's name may also
 * be null to save RAM since the name is currently not used.
 */
NIL_THREADS_TABLE_BEGIN()
NIL_THREADS_TABLE_ENTRY(NULL, CmdThread, NULL, waCmdThread, sizeof(waCmdThread))
NIL_THREADS_TABLE_ENTRY(NULL, TlmThread, NULL, waTlmThread, sizeof(waTlmThread))
NIL_THREADS_TABLE_ENTRY(NULL, IOThread, NULL, waIOThread, sizeof(waIOThread))
NIL_THREADS_TABLE_ENTRY(NULL, BlinkThread, NULL, waBlinkThread, sizeof(waBlinkThread))
NIL_THREADS_TABLE_END()

//------------------------------------------------------------------------------
void setup()
{
  Serial.begin(9600);  // initialize serial:
  pinMode(13, OUTPUT); // initialize digital pin 13 as an output.

  Cmd.InitializeApp(&Cmd, &Tlm);
  Tlm.InitializeApp(&Cmd, &Tlm);
  Blinkie.InitializeApp(&Cmd, &Tlm);

  nilSysBegin();
}

//------------------------------------------------------------------------------
// Loop is the idle thread.  The idle thread must not invoke any 
// kernel primitive able to change its state to not runnable.
void loop()
{
}
