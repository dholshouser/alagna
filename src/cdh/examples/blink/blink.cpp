#include "Arduino.h"
#include "blink.h"

Blink::Blink(apid_e apid) : Application(apid)
{
  Serial.print("Blink Construct\n");
  on = false;
}
void Blink::Initialize(void)
{
  Serial.print("Blink Initialize\n");
  // register pkts
  myBlinkSoh.delay = 250;
}

void Blink::Process()
{
  if(on)
  {
    digitalWrite(13, LOW);
    on = false;
  }
  else
  {
    digitalWrite(13, HIGH);
    on = true;
  }

  // TBD using delay for now. Upgrade to NilTimer
  nilThdSleepMilliseconds(myBlinkSoh.delay);
}

cmd_rsp_e Blink::ProcessCmd(pkt_t& pkt)
{
  cmd_rsp_e retVal = CMD_RSP_OK;
  switch(pkt.hdr.pktid)
  {
    case CMDID_CLRCTR:
      break;
    default:
      break;
  }
  return retVal;
}
