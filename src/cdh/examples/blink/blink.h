#ifndef __BLINK_H__
#define __BLINK_H__
#include "Arduino.h"
#include "application.h"

class Blink : public Application
{

private:
  typedef struct blink_soh_n
  {
    byte delay;
  } blink_soh_t;
  
  // Global variable to hold State of Health tlm packet.
  blink_soh_t myBlinkSoh;
  bool on;
  
public:
  Blink(apid_e apid);

  void Initialize(void);

  void Process();
  cmd_rsp_e ProcessCmd(pkt_t& pkt);
};

#endif
