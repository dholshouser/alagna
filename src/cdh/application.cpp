#include "Arduino.h"
#include "application.h"
#include "telemetry.h"
#include "command.h"

Application::Application(apid_e apid)
{
  myApid = apid;
  mySoh.cmds_acpt = 0;
  mySoh.cmds_rjct = 0;
  mySoh.cmds_rcvd = 0;
  mySoh.last_cmd_rsp = 0;
  mySoh.cycle_cnt = 0;
}

/**
 * Process is called by Execute() for priority based execution.
 * Application has a thread and calls Process each loop. Process
 * may block on a queue, mutex, semaphore, or timer but the thread
 * is contained within Application so the super class has a chance
 * to perform tasks.
 */
void Application::Execute()
{
  while(true)
  {
    mySoh.cycle_cnt++;
    Process();
  }
}

  /**
   * ProcessCmd is called when a new command is recieved by Command.
   * The idea here is that any OS interaction is abstracted from
   * the developer so the app doesn't need to know if the command
   * is handled by a Q, func*, or another mechanism. The cmd is 
   * registered during Setup()/Initialize().
   * This should eventually be upgraded so that new commands are sent
   * to a Q in Application by Command. 
   * Giovanni says that Chibi/RT mailboxes should be able to compile
   * for NIL. Upgrade this to mailboxes/Qs someday. That would mean
   * ExecuteCmd() would be another registered thread.
   */
cmd_rsp_e Application::ExecuteCmd(pkt_t &pkt)
{
  switch(pkt.hdr.pktid)
  {
    case CMDID_CLRCTR:
		  mySoh.cmds_acpt = 0;
		  mySoh.cmds_rjct = 0;
		  mySoh.cmds_rcvd = 0;
		  mySoh.cycle_cnt = 0;

      mySoh.last_cmd_rsp = CMD_RSP_OK;
    default:
		  mySoh.last_cmd_rsp = (byte) ProcessCmd(pkt);
			mySoh.cmds_rcvd++;

		  switch( mySoh.last_cmd_rsp)
		  {
		    case CMD_RSP_OK:
				  mySoh.cmds_acpt++;
		      break;
		    case CMD_RSP_RJCT:
		    case CMD_RSP_FAIL:
		    case CMD_RSP_PARAM:
				  mySoh.cmds_rjct++;
		      break;
		  }
      break;
  }
  return (cmd_rsp_e)mySoh.last_cmd_rsp;
}

void Application::InitializeApp(Application *Cmd, Application *Tlm)
{
  myTlm = (void*)Tlm;
  myCmd = (void*)Cmd;
  ////////////////////////////////////////////////////////////////////////////////
  // Register Application Telemetry 
  ((Telemetry*)myTlm)->RegisterTlm(myApid, TLMID_APPSOH, ( pkt_t*)&mySoh, sizeof(app_soh_t), 1000);
  ((Command*)myCmd)->RegisterCmd(myApid, CMDID_CLRCTR, (void*)this);
 
  // Register Application SOH pkt, etc.
  // cmds processed
  // execution cycles
  Initialize(); // Allow app to register for tlm.
}  


