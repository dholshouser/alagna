#ifndef __APPLICATION_H__
#define __APPLICATION_H__
#include "Arduino.h"
#include <NilRTOS.h>
#include "cdh.h"

class Application
{
  
  typedef struct app_soh_n
  {
    pkt_hdr_t hdr;
    int cmds_acpt;
    int cmds_rjct;
    int cmds_rcvd;
    int last_cmd_rsp;
    int cycle_cnt;
  } app_soh_t;
protected:
  void *myCmd;
  void *myTlm;  
public:
  apid_e myApid;
  
// Tlm pkt id. Each app has a set of ids.
// TBD: how to reserve Application ids, but have an app setup their own ids.
typedef enum app_tlm_id_n
{
  TLMID_APPSOH   = 0,
  TLMID_TLMSOH   = 1,
  TLMID_CMDSOH   = 1,
} app_tlm_id_e;

// Cmd pkt id. Each app has a set of ids.
typedef enum app_cmd_id_n
{
  CMDID_CLRCTR   = 0
} app_cmd_id_e;


  Application(apid_e apid);

	void Execute();
	cmd_rsp_e ExecuteCmd(pkt_t& pkt);
  void InitializeApp(Application *Cmd, Application *Tlm);
  
  virtual void Initialize(void)=0;
  virtual cmd_rsp_e ProcessCmd(pkt_t& pkt)=0;
  virtual void Process()=0;

  app_soh_t mySoh;
};
#endif
