#include "Arduino.h"
#include "telemetry.h"

Telemetry::Telemetry(apid_e apid) : Application(apid)
{
  //Serial.print("Telemetry::Telemetry()\n");
  myTlmSoh.tlm_registered = 0;
}
void Telemetry::Initialize(void)
{
  // register pkts
  RegisterTlm(myApid, TLMID_TLMSOH, (pkt_t*) &myTlmSoh, sizeof(tlm_soh_t), 1000);
}

void Telemetry::RegisterTlm(apid_e apid, byte pktid, pkt_t* pkt, byte size, int rate)
{
  if(myTlmSoh.tlm_registered<TLM_TABLE_SIZE)
  {
    TlmTable[myTlmSoh.tlm_registered].apid = apid;
    TlmTable[myTlmSoh.tlm_registered].pktid = pktid;
    TlmTable[myTlmSoh.tlm_registered].pkt = pkt;
    TlmTable[myTlmSoh.tlm_registered].size = size;
    TlmTable[myTlmSoh.tlm_registered].rate = rate;
    myTlmSoh.tlm_registered++;
  }
}

void Telemetry::Process()
{
  int now = millis();

// Loop through the TlmTable
// Convert time delta since last send of each pkt into hertz or something like that
// and check if it's time to send.
// The pkt is a pointer to the Apps pkt. It's up to the app to update the contents
// What about mutex protecting the contents of a packet? needed?
  for(int i=0; i<myTlmSoh.tlm_registered; i++)
  {
    if((now-TlmTable[i].last_send_time)> TlmTable[i].rate)
    {
      // The header in the pkt must be complete.
      // PushPkt uses the header values for loop control.
      TlmTable[i].pkt->hdr.apid = TlmTable[i].apid;
      TlmTable[i].pkt->hdr.pktid = TlmTable[i].pktid;
      TlmTable[i].pkt->hdr.length = TlmTable[i].size;
      if(PushPkt(TlmTable[i].pkt))
        TlmTable[i].last_send_time = now;
    }
  }

  // TBD using delay for now. Upgrade to NilTimer
  nilThdSleepMilliseconds(10);
}

cmd_rsp_e Telemetry::ProcessCmd(pkt_t& pkt)
{
  cmd_rsp_e retVal = CMD_RSP_OK;
  switch(pkt.hdr.pktid)
  {
    case CMDID_CLRCTR:
      myTlmSoh.bytes_sent = 0;
      break;
    default:
      break;
  }
  return retVal;
}

// TBD: Wrap the FIFO in a semaphore so Process() and SendPkt()
//      can both write to this FIFO.

// TBD: If the packet is not in use, mark it used.
//      This is for use to protect against a slow IO link. Can't
//      buffer a shit-ton of data, so the pkt itself is the buffer.
//      Gotta make some kind of logic to store in the pkt that
//      it's in use and not to write to it.
//
//      Eg: a camera app is taking pix and streaming them to IO.
//          The camera app breaks up each pic into a stream of pkts
//          and sends them to IO.
//          Maybe IO contains an array of max size pkts, but that 
//          takes up too much RAM.
//
//          Maybe the camera app has a small(2) array of packets so
//          it can be writing to one while IO is sending the other
//          down the pipe.
//
//          When the IO link is full, the 
//          camera app can only write to the next pkt when the pkt 
//          is no longer in use. TBD. Can't do much with insufficient
//          RAM. What other buffering scheme would work for a full
//          IO link?
bool Telemetry::PushPkt(pkt_t* pkt)
{
  pkt_t** p; // The FIFO is of type pkt* so returns pkt**

  // Get a pointer to the FIFO element
  p = TlmFifo.waitFree(TIME_IMMEDIATE);
  
  // Continue if no free space.
  // The FIFO is only 10 pkt* deep.
  // This will happen frequently and apps need to be written
  // to handle this gracefully. Ie: don't fail, just try again
  // if that's appropriate for the task.
  // For the standard TlmThread just don't update the last_send_time
  // and the next loop will try again.
  if (p == 0) return false;
  
  // Store pkt* in FIFO element.
  *p = pkt;

  // Signal FIFO to hold FIFO element.
  TlmFifo.signalData();
}

void Telemetry::ProcessTlm()
{
  pkt_t** p;  // The FIFO is of type pkt* so returns pkt**
  pkt_t* pkt; // Actual pkt* to read header.length.
  char* buf;  // Buffer of bytes send with Serial.write(buf[i])

  while(1)
  {
    // Wait forever for the next pkt.
    pkt_t** p = TlmFifo.waitData(TIME_INFINITE);
    
    // Continue if no data.
    // This must some kind error condition or isn't useful.
    if (!p) continue;
    
    // Fetch the pkt* copy it into the buffer
    pkt = *p;
    buf = (char*)*p;

    for(int i=0; i<pkt->hdr.length; i++)
    {
      Serial.write(buf[i]);
      myTlmSoh.bytes_sent++;
    }

    // Signal FIFO slot is free.
    TlmFifo.signalFree();
  }
}