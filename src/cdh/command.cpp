#include "Arduino.h"
#include "command.h"
#include "telemetry.h"
#include <NilTimer1.h>

//void Command::Command(Telemetry* nTlm, Command* nCmd, byte apid)
Command::Command(apid_e apid) : Application(apid)
{
  myCmdSoh.cmds_registered = 0;
}
void Command::Initialize(void)
{
  // register pkts
  ((Telemetry*)myTlm)->RegisterTlm(myApid, TLMID_CMDSOH, (pkt_t*) &myCmdSoh, sizeof(cmd_soh_t), 1000);
}

// Until I/O ISR, just read Serial during Cmd.Process() and send
// cmds to any registered apps.
void Command::Process()
{
  pkt_t pkt;
  
  while(readCmd((char*)&pkt)>0)
  {
    if(pkt.hdr.apid<NUM_APIDS)
    {
      for(int i=0; i<myCmdSoh.cmds_registered; i++)
      {
        if(CmdTable[i].apid == pkt.hdr.apid && CmdTable[i].pktid == pkt.hdr.pktid)
        {
          mySoh.cmds_rcvd++;
          if(((Application*)CmdTable[i].app)->ExecuteCmd(pkt) == CMD_RSP_OK)
          {
            myCmdSoh.cmds_acpt++;
          }
          else
          {
            myCmdSoh.cmds_rjct++;
          }
          myCmdSoh.cmds_rcvd++; 
        }
      }
    }
  }

  // TBD using delay for now. Upgrade to NilTimer
  nilThdSleepMilliseconds(100);
}

cmd_rsp_e Command::ProcessCmd(pkt_t &pkt)
{
  cmd_rsp_e retVal = CMD_RSP_OK;
  switch(pkt.hdr.pktid)
  {
    case CMDID_CLRCTR:
      myCmdSoh.bytes_rcvd = 0;
      myCmdSoh.cmds_acpt = 0;
      myCmdSoh.cmds_rjct = 0;
      myCmdSoh.cmds_rcvd = 0;
      break;
    default:
      break;
  }
  return retVal;
}

void Command::RegisterCmd(byte apid, byte pktid, void* app) 
{
  if(myCmdSoh.cmds_registered<CMD_TABLE_SIZE)
  {
    CmdTable[myCmdSoh.cmds_registered].apid = apid;
    CmdTable[myCmdSoh.cmds_registered].pktid = pktid;
    CmdTable[myCmdSoh.cmds_registered].app = app;
    myCmdSoh.cmds_registered++;
  }
}

byte Command::readCmd(char* pkt)
{
  int i=0;
  int avail = Serial.available();
  byte length = 0;
  
  // Read length, and then header, and then rest of pkt
  if (avail>0)
  {
    pkt[0] = Serial.read(); // Get length field and shove it into pkt header.
    // This could be expanded with better size/error checking.
    // Read the header first, then the data with size limits, etc.
    for(i=1; i<pkt[0]; i++)
    {
      pkt[i] = Serial.read(); 
      if(i>MAX_PKT_DATA_SIZE)
        break;
    }
  }
  return i;
}
