#ifndef __CDH_H__
#define __CDH_H__

#include "Arduino.h"

#define DEFAULT_THREAD_STACK_SIZE 96
#define CMD_TABLE_SIZE 32
#define TLM_TABLE_SIZE 32
#define MAX_PKT_DATA_SIZE 64

typedef enum apid_n
{
  APID_CMD   = 0,
  APID_TLM   = 1,
  APID_BLINK = 2,
  NUM_APIDS
} apid_e;


// This structure is used by Command and Telemetry.
// It is used for any packet based communication.
typedef struct pkt_hdr_n
{
  byte length; // TBD this limits all cmd & tlm pkts to 255 bytes, including header.
  byte apid;
  byte pktid;
  //int  chksum;
} pkt_hdr_t;

typedef struct pkt_n
{
  pkt_hdr_t hdr;
  byte data[MAX_PKT_DATA_SIZE]; // TBD, move to byte* data to conserve space. App must create data area.
} pkt_t;

typedef enum cmd_rsp_n
{
  CMD_RSP_OK = 0,
  CMD_RSP_RJCT = 1,
  CMD_RSP_FAIL = 2,
  CMD_RSP_PARAM = 3
} cmd_rsp_e;


#endif
