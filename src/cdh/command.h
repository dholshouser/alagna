#ifndef __COMMAND_H__
#define __COMMAND_H__
#include "Arduino.h"
#include "application.h"

class Command : public Application
{
  typedef struct cmd_soh_n
  {
    pkt_hdr_t hdr;
    byte cmds_registered;
    unsigned int bytes_rcvd;
    unsigned int cmds_acpt;
    unsigned int cmds_rjct;
    unsigned int cmds_rcvd;
  } cmd_soh_t;
  
  typedef struct cmd_table_t
  {
    void* app;
    byte apid;
    byte pktid;
  } cmd_table_t;

  cmd_soh_t myCmdSoh;
  cmd_table_t CmdTable[CMD_TABLE_SIZE];
  
// Tlm pkt id. Each app has a set of ids.
typedef enum tlm_id_n
{
  TLMID_APPSOH   = 0,
} tlm_id_e;

// Cmd pkt id. Each app has a set of ids.
typedef enum cmd_id_n
{
  CMDID_CLRCTR   = 0
} cmd_id_e;
public:
  Command(apid_e apid);

  //void ISR(); // TBD: connect interrupt to I/O device and the Execute method.
  void Initialize(void);
 
  // TBD This needs a size field.
  void RegisterCmd(byte apid, byte pktid, void* app);
  
  void Process();
  cmd_rsp_e ProcessCmd(pkt_t &pkt);
  byte readCmd(char* pkt);
};

#endif
