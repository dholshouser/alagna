4.upto(255) do |i|
  cmd("BLINK delay with DELAY #{i}")
  wait_check("BLINK SOH DELAY == #{i}", 5)
end
