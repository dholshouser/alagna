# Alagna / Command and Data Handling library #

Alagna is a standard Arduino library. Copy the contents of the [src](https://bitbucket.org/dholshouser/alagna/src/5189c6fa6adb/src/) directory into your Arduino libraries directory. 

Alagna depends on [NilRTOS](http://www.chibios.org/dokuwiki/doku.php?id=chibios:product:nil:start). Copy the contents of the NilRTOS [libraries](https://github.com/greiman/NilRTOS-Arduino/tree/master/libraries/) directory into your Arduino libraries directory.

### What is this repository for? ###

Alagna is a [Command and Data Handling (C&DH)](https://en.wikipedia.org/wiki/On-board_data_handling) architecture for Arduino that communicates with [COSMOS](http://cosmosrb.com/). Alagna receives command packets and sends telemetry packets. COSMOS is not required by any means, but the purpose of Alagna is to provide a drop-in capability to run real-time, multi-threaded projects on an Arduino, which are packet based and can communicate with [COSMOS](http://cosmosrb.com/) with minimal effort.

A C&DH architecture handles I/O (for communicating from your embedded device to your controller interface), mode based command filtering, command routing to applications, configuration management (ie: config files), filesystem management, and rate based telemetry (tlm) production, etc.

[COSMOS](http://cosmosrb.com/) is an embedded interface with real-time packet based communication to your embedded device (Alagna comes with a template [COSMOS](http://cosmosrb.com/) configuration to reduce your startup time). [COSMOS](http://cosmosrb.com/) can send commands, receive telemetry responses, has tools to do real-time and post processed graphing, telemetry display and playback with limits, limits responses, and much, much more.

# Current Status #
The current status of Alagna is very pre-alpha. 

* routes commands and produces rate based telemetry packets
* contains a blink example application
* contains an example [COSMOS](http://cosmosrb.com/) config
* contains a super class each application inherits which contains a standard State of Health packet and registers a command
* contains Command and Telemetry applications that inherit from Application
** Command and Telemetry run at 10Hz
* each application registers its telemetry packets at a give rate with the Telemetry class during system startup
* commands are routed to the prescribed applications class but are processed in the context of the Command class.
** This will be separated with message queues in ticket #9.
* the Telemetry class can produce packets, but can't yet handle asynchronous pushes, inter-App tlm requests, and has no mutex locking mechanism or buffer if the I/O pipe is backed up
** This will be upgraded in ticket #8.
* uses just over 1KB of RAM